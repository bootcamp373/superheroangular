import { Component, OnInit } from '@angular/core';
 import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Superhero } from '../Models/superhero.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SuperheroService } from '../providers/superhero.service';
import { map, Observable } from 'rxjs';
 import { NgControl } from '@angular/forms';
 import { NgForm } from '@angular/forms';
 import { NgModule } from '@angular/core';
 import { Form } from '@angular/forms';

@Component({
  selector: 'app-editsuperhero',
  templateUrl: './editsuperhero.component.html',
  styleUrls: ['./editsuperhero.component.css']
})

export class EditsuperheroComponent {
  superhero!: Superhero;
  id: number = 0;
  name: string = "";
   nickname: string = "";
  superpower: string = "";
  telephone: string = "";
  dateofbirth: Date = new Date();

  constructor(private activatedRoute: ActivatedRoute, private SuperheroService: SuperheroService, private router: Router) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.id = params['id']});
    this.SuperheroService.getSuperHeroById(this.id).subscribe((data) => {
      this.id = data.id;
      this.name = data.name;
        this.nickname = data.nickname;
        this.superpower = data.superpower;
        this.telephone = data.telephone;
        this.dateofbirth = data.dateofbirth;
    });
  }
  onClickSaveButton() {
    this.SuperheroService.updateSuperhero(new Superhero(this.id, this.name, this.nickname, this.superpower, this.telephone, this.dateofbirth)).subscribe(data => console.dir(data));
    alert("Record updated");
    this.router.navigate(['/Superhero']);
  }
}
