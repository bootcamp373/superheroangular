import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletesuperheroComponent } from './deletesuperhero.component';

describe('DeletesuperheroComponent', () => {
  let component: DeletesuperheroComponent;
  let fixture: ComponentFixture<DeletesuperheroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeletesuperheroComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DeletesuperheroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
