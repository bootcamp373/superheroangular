import { Component, OnInit } from '@angular/core';
import { Superhero } from '../Models/superhero.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SuperheroService } from '../providers/superhero.service';
import { map, Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-superhero',
  templateUrl: './superhero.component.html',
  styleUrls: ['./superhero.component.css']
})
export class SuperheroComponent implements OnInit {

  superhero?:  Superhero[];

  constructor(private superheroService: SuperheroService, private router: Router) {}
  ngOnInit() {

    this.superheroService.getallSuperheroes()
    .subscribe(a => this.superhero = a);
  }
  // private httpOptions = {
  //   headers: new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Access-Control-Allow-Origin': '*',
  //     'Referrer-Policy': 'strict-origin-when-cross-origin',
  //   }),
  // };
}


