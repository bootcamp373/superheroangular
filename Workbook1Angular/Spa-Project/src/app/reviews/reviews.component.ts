import { Component, OnInit } from '@angular/core';
import { Category } from '../models/category.model';
import { CategoriesService } from '../providers/categories.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Reviews } from '../models/reviews.model';
import { ReviewsService } from '../providers/reviews.service';


@Component({
  selector: 'app-search',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})

export class ReviewsComponent implements OnInit {
  Reviews: Array<Reviews> = [];
  constructor(private ReviewsService: ReviewsService) { }
  ngOnInit() {
    // call getToDos() method in ToDoService
    this.ReviewsService.getReviews().subscribe(data => {
      this.Reviews = data;
      console.dir(this.Reviews);
    });
  }
}
