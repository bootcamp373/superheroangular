import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Superhero } from '../Models/superhero.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SuperheroService } from '../providers/superhero.service';
import { map, Observable } from 'rxjs';
 import { NgControl } from '@angular/forms';
 import { NgForm } from '@angular/forms';
 import { NgModule } from '@angular/core';
 import { Form } from '@angular/forms';


@Component({
  selector: 'app-superheroadd',
  templateUrl: './superheroadd.component.html',
  styleUrls: ['./superheroadd.component.css']
})



export class superheroaddcomponent   {
   newSuperhero: Superhero = new Superhero(0, "", "", "", "", new Date());

  constructor(private activatedRoute: ActivatedRoute, private superheroService: SuperheroService, private router: Router)  {


  }
 
  onClickAddButton() {

    this.superheroService.postNewSuperhero(this.newSuperhero).subscribe(data => console.dir(data));
    this.router.navigate(['/Superhero']);
  }
}



