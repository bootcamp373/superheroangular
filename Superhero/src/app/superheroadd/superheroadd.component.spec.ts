import { ComponentFixture, TestBed } from '@angular/core/testing';

import { superheroaddcomponent } from './superheroadd.component';

describe('SuperheroaddComponent', () => {
  let component: superheroaddcomponent;
  let fixture: ComponentFixture<superheroaddcomponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ superheroaddcomponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(superheroaddcomponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
