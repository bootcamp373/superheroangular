import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SuperheroComponent } from './superhero/superhero.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { superheroaddcomponent } from './superheroadd/superheroadd.component';
import { FormBuilder, FormsModule } from '@angular/forms';
import { EditsuperheroComponent } from './editsuperhero/editsuperhero.component';
import { DeletesuperheroComponent } from './deletesuperhero/deletesuperhero.component';


const routes: Routes = [
  { path: "Superhero", component: SuperheroComponent },
  { path: "superheroadd", component: superheroaddcomponent },
  { path: "editsuperhero", component: EditsuperheroComponent },
  { path: "deletesuperhero", component: DeletesuperheroComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    SuperheroComponent,
    superheroaddcomponent,
    EditsuperheroComponent,
    DeletesuperheroComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
