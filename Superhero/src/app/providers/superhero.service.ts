import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Superhero } from '../Models/superhero.model';

@Injectable({
  providedIn: 'root'
})
export class SuperheroService {

  constructor(private http: HttpClient, ) { };
  

  // set up the endpoint and any HTTP headers
  private SuperheroEndpoint: string =
    'https://localhost:7233/api/Superhero';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getallSuperheroes(): Observable<Superhero[]> {
    return this.http.get<Superhero[]>(this.SuperheroEndpoint,
      this.httpOptions)
      .pipe(map(res => <Superhero[]>res));
  }

  getSuperHeroById(id: number): Observable<Superhero> {
    return this.http
      .get('https://localhost:7233/api/Superhero/'+ id, this.httpOptions)
      .pipe(map((res) => <Superhero>res));
  }


  postNewSuperhero(superhero: Superhero): Observable<Superhero> {
    return this.http.post<Superhero>('https://localhost:7233/api/Superhero',
      {
        id: superhero.id,
        name: superhero.name,
        nickname: superhero.nickname,
        superpower: superhero.superpower,
        telephone: superhero.telephone,
        dateofbirth: superhero.dateofbirth    
      },
      this.httpOptions);
  }
  updateSuperhero(superhero: Superhero): Observable<Superhero> {
    return this.http.put<Superhero>('https://localhost:7233/api/Superhero/' + superhero.id,
      {
        id: superhero.id,
        name: superhero.name,
        nickname: superhero.nickname,
        superpower: superhero.superpower,
        telephone: superhero.telephone,
        dateofbirth: superhero.dateofbirth 
      },
      this.httpOptions);
  }
  // deleteSuperhero(id: number): Observable<Superhero> {
    deleteSuperhero(id: number) {

    console.log(id, "deletesuperhero");
     return this.http.delete<Superhero>('https://localhost:7233/api/Superhero/' + id,
       this.httpOptions);
  }



}
